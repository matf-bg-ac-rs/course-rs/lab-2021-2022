#include <string>
#include <vector>
#include <algorithm>
#include <numeric>
#include <iterator>
#include <iostream>

class Product
{
private:
    std::string _pno;
    double _price;

public:
    Product(std::string &&pno, double price)
        : _pno(std::move(pno)), _price(price)
    {
    }

    double Price() const
    {
        return _price;
    }
};

class Box
{
private:
    bool _opened;

public:
    Box(bool opened)
        : _opened(opened)
    {
    }
};

class GlassBox : public Box
{
public:
    GlassBox(bool opened)
        : Box(opened)
    {
    }
};

class ProductList
{
private:
    std::vector<Product> _products;

protected:
    ProductList(std::vector<Product> &&products)
        : _products(std::move(products))
    {
    }

public:
    void AddProduct(Product p)
    {
        _products.push_back(p);
    }

    double Price() const
    {
        return std::transform_reduce(
            std::cbegin(_products),
            std::cend(_products),
            double{},
            std::plus<double>(),
            [](const auto &product){ return product.Price(); });
    }
};

class WrappingPaper : public ProductList
{
private:
    std::string _patternNo;

public:
    WrappingPaper(std::vector<Product> &&products, std::string &&patternNo)
        : ProductList(std::move(products)), _patternNo(patternNo)
    {
    }
};

class CartonBox : public Box, public ProductList
{
private:
    std::string _color;

public:
    CartonBox(bool opened, std::vector<Product> &&products, std::string &&color = "neutral")
        : Box(opened), ProductList(std::move(products)), _color(std::move(color))
    {
    }
};

int main()
{
    WrappingPaper yellowPaper({ Product("picture-frame-1", 5.99), Product("old-pen-4", 14.99) }, "yellow-1");
    std::cout << yellowPaper.Price() << std::endl;

    CartonBox blueCartonBox(false, {}, "blue");
    blueCartonBox.AddProduct(Product("old-shirt-with-flowers-1", 13.99));
    blueCartonBox.AddProduct(Product("denim-jeans-3", 26.99));
    std::cout << blueCartonBox.Price() << std::endl;

    return 0;
}
