#include "Zaposleni.hpp"

Zaposleni::Zaposleni(QString imePrezime, Pozicije pozicija)
    : _imePrezime(imePrezime)
    , _pozicija(pozicija)
{

}

QString Zaposleni::NazivPozicije() const
{
    switch (_pozicija)
    {
    case Pozicije::Sefovi:
        return "Sefovi";
    case Pozicije::Radnici:
        return "Radnici";
    default:
        throw QString("Nepoznata pozicija");
    }
}
