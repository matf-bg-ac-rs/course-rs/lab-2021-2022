#ifndef ZAPOSLENI_HPP
#define ZAPOSLENI_HPP

#include <QString>

class Zaposleni
{
public:
    enum class Pozicije
    {
        Sefovi,
        Radnici
    };

    Zaposleni(QString imePrezime, Pozicije pozicija);

    inline QString ImePrezime() const
    {
        return _imePrezime;
    }

    inline Pozicije Pozicija() const
    {
        return _pozicija;
    }

    QString NazivPozicije() const;

private:
    QString _imePrezime;
    Pozicije _pozicija;
};

#endif // ZAPOSLENI_HPP
