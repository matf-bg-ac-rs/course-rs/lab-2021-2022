#include <QGraphicsLineItem>
#include "Tabla.hpp"
#include "Zaposleni.hpp"
#include "CvorZaposleni.hpp"

Tabla::Tabla(QObject *parent)
    : QGraphicsScene(parent)
{

}

void Tabla::DodatZaposleniNaTablu(CvorZaposleni *cvor)
{
    if (cvor->Zaposlen()->Pozicija() == Zaposleni::Pozicije::Sefovi) {
        _sefovi.append(cvor);
    }
    else if (cvor->Zaposlen()->Pozicija() == Zaposleni::Pozicije::Radnici) {
        _radnici.append(cvor);
    }

    PozicionirajNoviCvor(cvor);
    NacrtajNoveVeze();
}

void Tabla::PozicionirajNoviCvor(CvorZaposleni *cvor)
{
    const auto sirinaTable = static_cast<int>(this->width());
    auto yPozPomeraj = 0.0;
    auto indeksNovogCvora = 0;

    if (cvor->Zaposlen()->Pozicija() == Zaposleni::Pozicije::Sefovi) {
        indeksNovogCvora = _sefovi.size() - 1;
    }
    else if (cvor->Zaposlen()->Pozicija() == Zaposleni::Pozicije::Radnici) {
        yPozPomeraj = height() / 2;
        indeksNovogCvora = _radnici.size() - 1;
    }

    const auto xPoz = (cvor->Sirina() * indeksNovogCvora) % sirinaTable;
    const auto yPoz = cvor->Visina() * ((cvor->Sirina() * indeksNovogCvora) / sirinaTable);
    cvor->setPos(xPoz, yPoz + yPozPomeraj);
}

void Tabla::NacrtajNoveVeze()
{
    for (auto veza : _veze)
    {
        this->removeItem(veza);
        delete veza;
    }
    _veze.clear();

    for (const auto sef : _sefovi)
    {
        for (const auto radnik : _radnici)
        {
            QLineF linija(sef->PozicijaCentraDna(), radnik->PozicijaCentraVrha());
            auto veza = new QGraphicsLineItem(linija);
            _veze.append(veza);
            this->addItem(veza);
        }
    }
}
