#include <iostream>
#include "proveriUredjenost.hpp"

using namespace std;

int main()
{
     const std::string text = "Hooloovoo";
     std::cerr << text << ": " << proveriUredjenost(text) << std::endl;

     const std::string word = "AlmosT";
     std::cerr << word << ": " << proveriUredjenost(word) << std::endl;

     const std::vector<int> numbers{1, 2, 3, 3, 4, 5, 6, 6, 7};
     std::cerr << "numbers: " << proveriUredjenost(numbers) << std::endl;

     return 0;
}
