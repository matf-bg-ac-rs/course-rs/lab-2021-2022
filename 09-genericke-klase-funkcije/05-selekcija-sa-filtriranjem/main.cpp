#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>

namespace matf
{
    template <typename It, typename Pred>
    void selekcijaSaFiltriranjem(It pocetak, It kraj, It odrediste, Pred predikat)
    {
        std::stable_partition(pocetak, odrediste, [predikat](const auto &element)
                              { return !predikat(element); });
        std::stable_partition(odrediste, kraj, predikat);
    }
}

int main()
{
    std::vector<std::string> stavke{
        "aardvark",
        "compunctious",
        "**congratulations**",
        "contrafribblarity",
        "contrary",
        "dictionary", // +5 --> odrediste
        "**interphrastical**",
        "**patronise**",
        "**pericombobulation**",
        "phrasmotic",
        "**syllables**"};

    matf::selekcijaSaFiltriranjem(
        std::begin(stavke),
        std::end(stavke),
        std::begin(stavke) + 5,
        [](const auto &stavka)
        { return stavka[0] == '*'; });

    std::copy(std::begin(stavke),
              std::end(stavke),
              std::ostream_iterator<std::string>(std::cout, "\n"));

    return 0;

    return 0;
}
