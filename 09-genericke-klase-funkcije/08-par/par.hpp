#pragma once

#include <iostream>

namespace matf
{
    template <typename T1, typename T2>
    class par
    {
    public:
        par(T1 t1, T2 t2)
            : prvaVrednost(t1), drugaVrednost(t2)
        {
        }

        par() = default;

        void zameni(par<T1, T2> &drugi)
        {
            std::swap(prvaVrednost, drugi.prvaVrednost);
            std::swap(drugaVrednost, drugi.drugaVrednost);
        }

        T1 prvaVrednost;
        T2 drugaVrednost;
    };

    template <typename T1, typename T2>
    par<T1, T2> napravi_par(T1 t1, T2 t2)
    {
        return par<T1, T2>(t1, t2);
    }

} // namespace matf
