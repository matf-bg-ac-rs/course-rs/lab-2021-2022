#include <iostream>
#include <string>
#include "par.hpp"

int main()
{
    auto p1 = matf::napravi_par("Matematicki fakultet", 20);
    std::cout << "Tekst \"" << p1.prvaVrednost << "\" ima " << p1.drugaVrednost << " karaktera." << std::endl;

    auto p2 = matf::napravi_par("Tekst", 5);
    std::cout << "Pre zamene: "
              << std::endl
              << "p1: (" << p1.prvaVrednost << ',' << p1.drugaVrednost << ')' << std::endl
              << "p2: (" << p2.prvaVrednost << ',' << p2.drugaVrednost << ')' << std::endl;
    p1.zameni(p2);
    std::cout << "Nakon zamene: "
              << std::endl
              << "p1: (" << p1.prvaVrednost << ',' << p1.drugaVrednost << ')' << std::endl
              << "p2: (" << p2.prvaVrednost << ',' << p2.drugaVrednost << ')' << std::endl;

    return 0;
}
