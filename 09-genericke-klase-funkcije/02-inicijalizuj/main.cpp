#include <iostream>
#include <vector>

namespace matf
{
    // Funkcija `inicijalizuj` (inspirisano sa `std::iota`) inicijalizuje
    // elemente prosledjene kolekcije podataka na value, ++value, ...
    template <typename ForwardIterator, typename T>
    void inicijalizuj(ForwardIterator begin, ForwardIterator end, T value)
    {
        while (begin != end)
        {
            *begin++ = value++;
        }
    }
}

int main()
{
    // Inicijalizacija vektora rednim brojevima pocevsi od 1.
    // Primetite da smo pri alokaciji vektora prosledili broj 5,
    // cime smo unapred konstruisali pet "podrazumevanih" `int` vrednosti u vektoru
    // (te vrednosti su, naravno, celobrojne nule).
    std::vector<int> v(5u);

    matf::inicijalizuj(v.begin(), v.end(), 1);
    for (const auto &i : v)
    {
        std::cout << i << " ";
    }
    std::cout << std::endl;

    return 0;
}
