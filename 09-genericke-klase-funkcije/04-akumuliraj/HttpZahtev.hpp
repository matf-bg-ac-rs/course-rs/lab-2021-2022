#pragma once

#include <map>
#include <string>
#include <iostream>

class HttpZahtev
{
public:
    void dodajSadrzaj(std::string deoZahteva, std::string sadrzaj)
    {
        _sadrzaj.insert(std::make_pair(deoZahteva, sadrzaj));
    }

    friend std::ostream &operator<<(std::ostream &out, const HttpZahtev &httpZahtev)  
    {
        for (const auto [deoZahteva, sadrzaj] : httpZahtev._sadrzaj)
        {
            out << '\t' << deoZahteva << ": " << sadrzaj << std::endl;
        }
        return out;
    }

private:
    std::map<std::string, std::string> _sadrzaj;
};
