#include <iostream>
#include <vector>
#include <numeric>
#include "HttpZahtev.hpp"
#include "FunkcijaSrednjegSloja.hpp"

namespace matf
{
    // Funkcija `akumuliraj` vrsi akumuliranje vrednosti kroz kolekciju podataka
    // koristeci operator '+' i koristeci neutral `init`.
    template <typename ForwardIterator, typename T>
    T akumuliraj(ForwardIterator first, ForwardIterator last, T init)
    {
        for (; first != last; ++first)
        {
            init = std::move(init) + *first;
        }
        return init;
    }
}

int main()
{
    std::vector<int> xs{1, 2, 3, 4, 5};

    std::cout << "Suma std::accumulate: " << std::accumulate(xs.begin(), xs.end(), 0) << std::endl;
    std::cout << "Suma matf::akumuliraj: " << matf::akumuliraj(xs.begin(), xs.end(), 0) << std::endl;

    std::vector<FunkcijaSrednjegSloja> obradaZahteva {
        FunkcijaSrednjegSloja("Zaglavlja", "Content-Type: application/json"),
        FunkcijaSrednjegSloja("Telo", "{\"ime\":\"Pera\",\"prezime\":\"Peric\"}")
    };
    std::cout << "Obrada HTTP zahteva pomocu matf::akumuliraj:" << std::endl;
    std::cout << matf::akumuliraj(obradaZahteva.cbegin(), obradaZahteva.cend(), HttpZahtev{});

    return 0;
}
