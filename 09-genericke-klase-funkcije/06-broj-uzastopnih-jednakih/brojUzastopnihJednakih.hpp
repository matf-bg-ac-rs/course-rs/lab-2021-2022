#pragma once

#include <iostream>
#include <string>
#include <numeric>
#include <vector>
#include <iterator>

using namespace std;

// S obzirom da nam je sablonski tip Kolekcija nekakva STL struktura podataka,
// ako zelimo da "izvucemo" koji je sablonski tip jednog elementa kolekcije,
// to mozemo uraditi tako sto iskoristimo vrednost Kolekcija::value_type.
// Medjutim, zbog nacina kako sabloni rade,
// potrebno je da taj sablonski parametar takodje imenujemo,
// sto se radi sintaksom: "typename Element = typename Kolekcija::value_type"
template <typename Kolekcija,
          typename Element = typename Kolekcija::value_type>
int brojUzastopnihJednakih(const Kolekcija &brojevi)
{
    return inner_product(
        cbegin(brojevi),
        cend(brojevi) - 1,
        cbegin(brojevi) + 1,
        int(),
        [](const auto &akumulator, const auto &broj)
        { return akumulator + broj; },
        [](const auto &leviBroj, const auto &desniBroj)
        { return leviBroj == desniBroj ? 1 : 0; });
}
