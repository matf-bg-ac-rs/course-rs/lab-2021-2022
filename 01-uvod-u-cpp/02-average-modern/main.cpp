#include <iostream>
#include <vector>
#include <complex>
#include <numeric>
#include <algorithm>
#include <iterator>

// U ovom primeru cemo pokazati kako je moguce implementirati prethodni primer
// koriscenjem modernih elemenata standardne biblioteke C++ jezika.
// Cilj ovog primera nije da usvojimo sve nove elemente koje vidimo,
// vec da razumemo da C++ jezik ima svoje specificnosti u odnosu na druge jezike koje znamo do sada (npr. Java),
// te da je potrebno da posvetimo ozbiljnu kolicinu vremena kako bismo ga savladali u nastavku kursa.
// Drugim recima, prikazujemo kako bi jedan C++ programer trebalo da implementira prethodni primer.

template<typename C, typename T = typename C::value_type>
void read_print_average(C& numbers)
{
    // Uvozimo sva imena iz prostora imena "std" u doseg ove funkcije.
    // Ovo je mnogo bolje resenje nego da uvozimo u globalni opseg.
    using namespace std;

    // Citanje sa standardnog ulaza tako sto se "kopiraju kompleksni brojevi" u vektor.
    copy(istream_iterator<T>(std::cin), istream_iterator<T>(), back_inserter(numbers));

    // Ispisivanje na standardni izlaz tako sto se "kopiraju kompleksni brojevi" na izlazni tok.
    copy(cbegin(numbers), cend(numbers), ostream_iterator<T>(cout, "\n"));

    std::cout << "Aritmeticka sredina: " << std::accumulate(numbers.cbegin(), numbers.cend(), T()) / static_cast<T>(numbers.size()) << std::endl;
}

int main()
{
    // Uvozimo sva imena iz prostora imena "std" u doseg ove funkcije.
    // Ovo je mnogo bolje resenje nego da uvozimo u globalni opseg.
    using namespace std;

    vector<complex<double>> numbers;
    read_print_average(numbers);

    return 0;
}
