#pragma once 

#include <string>
#include <vector>
#include <algorithm>
#include <iterator>
#include <iostream>

using namespace std;

class NGram
{
public:
    NGram(string tekst, unsigned brojPonavljanja)
        : _tekst(tekst)
        , _brojPonavljanja(brojPonavljanja)
    {}

    inline string tekst() const
    {
        return _tekst;
    }

    inline void uvecajBrojPonavljanja()
    {
        ++_brojPonavljanja;
    }

    inline bool operator==(const NGram &ngram) const
    {
        return _tekst == ngram._tekst && _brojPonavljanja == ngram._brojPonavljanja;
    }

private:
    friend ostream& operator<<(ostream& izlaz, const NGram& ngram);

    string _tekst;
    unsigned _brojPonavljanja;
};

class NGramKalkulator
{
public:
    NGramKalkulator(string tekst)
        : _tekst(tekst)
    {
        validirajTekst(tekst);
    }

    vector<NGram> operator()(unsigned duzina) const
    {
        validirajDuzinu(duzina);
        vector<NGram> ngrami;

        for (auto i = 0u; i <= _tekst.size() - duzina; ++i)
        {
            const auto ngramTekst = _tekst.substr(i, duzina);
            obradiNGramTekst(ngramTekst, ngrami);
        }

        return ngrami;
    }

private:
    inline void obradiNGramTekst(const string ngramTekst, vector<NGram> &ngrami) const
    {
        auto ngram = find_if(begin(ngrami), end(ngrami), [&](const auto& ngram)
        {
            return ngram.tekst() == ngramTekst;
        });

        if (ngram == end(ngrami))
        {
            ngrami.emplace_back(ngramTekst, 1u);
        }
        else
        {
            ngram->uvecajBrojPonavljanja();
        }
    }

    inline void validirajTekst(string tekst)
    {
        if (tekst.empty())
        {
            throw "NGramKalkulator ne moze biti konstruisan iz praznog teksta";
        }
    }

    inline void validirajDuzinu(unsigned duzina) const
    {
        if (duzina > _tekst.size())
        {
            throw "Duzina n-grama ne moze biti veca od duzine teksta";
        }
    }

    string _tekst;
};

ostream& operator<<(ostream& izlaz, const NGram& ngram) 
{
    return izlaz << '(' << ngram._tekst << ',' << ngram._brojPonavljanja << ')';
}
