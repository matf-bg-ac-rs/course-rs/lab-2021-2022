#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <stdexcept>
#include <optional>
#include "LokatorSupermarketa.hpp"

TEST_CASE("LokatorSupermarketa", "[class]")
{
    SECTION("Konstruktor LokatorSupermarketa uspesno konstruise nasumicni objekat")
    {
        // Arrange
        const auto x = 0.0;
        const auto y = 0.0;

        // Act + Assert
        REQUIRE_NOTHROW(LokatorSupermarketa(x, y));
    }

    SECTION("Metod dodajPorodicu uspesno pamti jednu novu porodicu")
    {
        // Arrange
        LokatorSupermarketa lokator(0.0, 0.0);
        const Porodica p1(3.0, 4.0);
        const auto ocekivanRezultat = 1;

        // Act
        const auto dobijenRezultat = lokator.dodajPorodicu(p1);

        // Assert
        REQUIRE(dobijenRezultat == ocekivanRezultat);
    }

    SECTION("Metod dodajPorodicu ne pamti duplikate porodica")
    {
        // Arrange
        LokatorSupermarketa lokator(0.0, 0.0);
        const Porodica p1(3.0, 4.0);
        const auto p2 = p1;
        const auto ocekivanRezultat = 1;

        // Act
        lokator.dodajPorodicu(p1);
        const auto dobijenRezultat = lokator.dodajPorodicu(p2);

        // Assert
        REQUIRE(dobijenRezultat == ocekivanRezultat);
    }

    SECTION("Operator() prijavljuje izuzetak tipa invalid_argument ukoliko se prosledi kvadrant koji nije 1, 2, 3 ili 4")
    {
        // Arrange
        LokatorSupermarketa lokator(0.0, 0.0);

        // Act + Assert
        REQUIRE_THROWS_AS(lokator(5), invalid_argument);
    }

    SECTION("Operator() vraca prazan optional ukoliko lokator nema porodica")
    {
        // Arrange
        LokatorSupermarketa lokator(0.0, 0.0);
        const optional<double> ocekivanRezultat;

        // Act
        const auto dobijenRezultat = lokator(1);

        // Assert
        REQUIRE(dobijenRezultat.has_value() == ocekivanRezultat.has_value());
    }

    SECTION("Operator() vraca rastojanje do jedne porodice iz ispravnog kvadranta koja je jedina dodata")
    {
        // Arrange
        LokatorSupermarketa lokator(0.0, 0.0);
        lokator.dodajPorodicu(Porodica(3.0, 4.0));
        const optional<double> ocekivanRezultat(5.0);

        // Act
        const auto dobijenRezultat = lokator(1);

        // Assert
        CHECK(dobijenRezultat.has_value() == ocekivanRezultat.has_value());
        REQUIRE(dobijenRezultat.value() == ocekivanRezultat.value());
    }

    SECTION("Operator() vraca prazan optional za lokator koji sadrzi jednu porodicu iz neispravnog kvadranta")
    {
        // Arrange
        LokatorSupermarketa lokator(0.0, 0.0);
        lokator.dodajPorodicu(Porodica(3.0, 4.0));
        const optional<double> ocekivanRezultat;

        // Act
        const auto dobijenRezultat = lokator(2);

        // Assert
        REQUIRE(dobijenRezultat.has_value() == ocekivanRezultat.has_value());
    }

    SECTION("Operator() vraca ukupno rastojanje za lokator koji sadrzi vise od jedne porodice iz ispravnog kvadranta i makar jednu porodicu iz neispravnog kvadranta")
    {
        // Arrange
        LokatorSupermarketa lokator(0.0, 0.0);
        lokator.dodajPorodicu(Porodica(3.0, 4.0));
        lokator.dodajPorodicu(Porodica(-3.0, 4.0));
        lokator.dodajPorodicu(Porodica(6.0, 8.0));
        const optional<double> ocekivanRezultat(15.0);

        // Act
        const auto dobijenRezultat = lokator(1);

        // Assert
        REQUIRE(dobijenRezultat.has_value() == ocekivanRezultat.has_value());
        REQUIRE(dobijenRezultat.value() == ocekivanRezultat.value());
    }
}
