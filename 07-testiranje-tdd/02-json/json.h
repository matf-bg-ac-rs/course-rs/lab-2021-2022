#pragma once

#include <vector>
#include <map>
#include <string>
#include <iterator>
#include <stdexcept>

using namespace std;

string json(const vector<map<string, string>> &objekti)
{
    if (objekti.empty())
    {
        throw invalid_argument("objekti");
    }

    string rezultat = "[";

    for (auto i = 0u; i < objekti.size(); ++i)
    {
        if (objekti[i].empty())
        {
            rezultat += "{},";
            continue;
        }

        rezultat += '{';
        for (auto iter = cbegin(objekti[i]); iter != cend(objekti[i]); ++iter)
        {
            rezultat += '"';
            rezultat += iter->first;
            rezultat += '"';
            rezultat += ':';
            rezultat += '"';
            rezultat += iter->second;
            rezultat += '"';
            rezultat += ',';
        }
        rezultat.back() = '}';
        rezultat += ',';
    }

    rezultat.back() = ']';

    return rezultat;
}