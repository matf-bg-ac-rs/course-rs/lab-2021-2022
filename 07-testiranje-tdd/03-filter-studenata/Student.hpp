#pragma once

#include <string>

using namespace std;

class Student
{
public:
    Student(string ime, string prezime, double prosecnaOcena)
        : _ime(ime)
        , _prezime(prezime)
        , _prosecnaOcena(prosecnaOcena)
    {}

    inline string imePrezime() const
    {
        return _ime + " " + _prezime;
    }

    inline double prosecnaOcena() const
    {
        return _prosecnaOcena;
    }

private:
    string _ime;
    string _prezime;
    double _prosecnaOcena;
};
