
#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include "prebrajanje.h"

#include <vector>

TEST_CASE("Prebrajanje", "[function]")
{
    // Specijalni slucajevi

    SECTION("Za praznu kolekciju, funkcija vraca -1")
    {
        // Arrange
        const vector<int> ulaz;
        const auto ocekivanIzlaz = -1;

        // Act
        const auto dobijenIzlaz = prebrajanje(ulaz);

        // Assert
        REQUIRE(ocekivanIzlaz == dobijenIzlaz);
    }

    SECTION("Za kolekciju sa jednim elementom, funkcija vraca 0")
    {
        // Arrange
        const vector<int> ulaz{1};
        const auto ocekivanIzlaz = 0;

        // Act
        const auto dobijenIzlaz = prebrajanje(ulaz);

        // Assert
        REQUIRE(ocekivanIzlaz == dobijenIzlaz);
    }

    SECTION("Za kolekciju sa neopadajucim elementima, funkcija vraca 0")
    {
        // Arrange
        const vector<int> ulaz{1, 2, 3, 3, 3, 5, 17, 17, 29};
        const auto ocekivanIzlaz = 0;

        // Act
        const auto dobijenIzlaz = prebrajanje(ulaz);

        // Assert
        REQUIRE(ocekivanIzlaz == dobijenIzlaz);
    }

    SECTION("Za kolekciju sa opadajucim elementima, funkcija vraca duzinu kolekcije umanjenu za 1")
    {
        // Arrange
        const vector<int> ulaz{25, 15, 13, 4, 3, 0, -5, -6};
        const auto ocekivanIzlaz = static_cast<int>(ulaz.size() - 1);

        // Act
        const auto dobijenIzlaz = prebrajanje(ulaz);

        // Assert
        REQUIRE(ocekivanIzlaz == dobijenIzlaz);
    }

    // Uobicajeni slucajevi

    SECTION("Za kolekciju brojeva sa jednim opadajucim podnizom, funkcija vraca duzinu tog podniza umanjenog za jedan")
    {
        // Arrange
        const vector<int> ulaz{1, 2, 3, 2, 1, 2, 3};
        const auto ocekivanIzlaz = 2;

        // Act
        const auto dobijenIzlaz = prebrajanje(ulaz);

        // Assert
        REQUIRE(ocekivanIzlaz == dobijenIzlaz);
    }

    SECTION("Za kolekciju brojeva sa vise opadajucih podniza, funkcija vraca duzinu svih tih podniza umanjenu za broj podnizova")
    {
        // Arrange
        const vector<int> ulaz{1, 2, 3, 2, 1, 2, 3, 2, 1, 0, -1, 2};
        const auto ocekivanIzlaz = 6;

        // Act
        const auto dobijenIzlaz = prebrajanje(ulaz);

        // Assert
        REQUIRE(ocekivanIzlaz == dobijenIzlaz);
    }
}
