#pragma once

#include <vector>

using namespace std;

int prebrajanje(const vector<int> &xs)
{
    if (xs.empty())
        return -1;

    int rezultat = 0;
    int tmp = xs[0];

    for (auto x : xs)
    {
        if (x < tmp)
        {
            rezultat++;
        }
        tmp = x;
    }

    return rezultat;
}
