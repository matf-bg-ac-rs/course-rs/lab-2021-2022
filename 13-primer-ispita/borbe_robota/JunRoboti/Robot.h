#ifndef ROBOT_H
#define ROBOT_H

#include <QString>
#include <QVariant>
#include <QMutex>

class Robot
{
public:
    Robot();

    void fromQVariant(const QVariant &variant);
    QString toQString() const;
    bool attack(Robot &target) const;

    void resetHitPoints();
    QMutex *getBattleMutex();

    QString getName() const;

private:
    QString m_name;
    unsigned m_minAttack, m_maxAttack;
    unsigned m_maxHitPoints, m_currentHitPoints;
    QMutex m_battleMutex;
};

#endif // ROBOT_H
