#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QList>

#include "Robot.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void onLoadClicked();
    void onBattleClicked();

private:
    void loadRobots();
    void fillRobotList();

    void initBattleTable();
    void startThreads();
    void setBattleWinner(int winner, int loser);
    void setCompetitionWinner();

    Ui::MainWindow *ui;
    QList<Robot *> m_robots;
    int m_numOfRemainingBattles = 0;
};
#endif // MAINWINDOW_H
