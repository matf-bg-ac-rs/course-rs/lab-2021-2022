#ifndef ROBOTWORKER_H
#define ROBOTWORKER_H

#include <QThread>
#include <QObject>

#include "Robot.h"

class RobotWorker : public QThread
{
    Q_OBJECT

public:
    RobotWorker(Robot *robotOne, Robot *robotTwo, int i, int j);

protected:
    void run() override;

signals:
    void battleFinished(int winner, int loser);

private:
    Robot *m_robotOne, *m_robotTwo;
    int m_i, m_j;
};

#endif // ROBOTWORKER_H
