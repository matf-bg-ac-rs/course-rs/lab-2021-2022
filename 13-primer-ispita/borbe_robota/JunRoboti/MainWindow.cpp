#include "MainWindow.h"
#include "./ui_MainWindow.h"
#include "RobotWorker.h"

#include <QFile>
#include <QFileDialog>
#include <QJsonDocument>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->loadRobotsBtn, &QPushButton::clicked, this, &MainWindow::onLoadClicked);
    connect(ui->startBattleBtn, &QPushButton::clicked, this, &MainWindow::onBattleClicked);
}

MainWindow::~MainWindow()
{
    qDeleteAll(m_robots);
    delete ui;
}

void MainWindow::onLoadClicked()
{
    loadRobots();
    fillRobotList();
}

void MainWindow::loadRobots()
{
    QString fileName = QFileDialog::getOpenFileName(this, "Select robot file", "", "*.json");
    if (fileName.isEmpty())
        return;

    QFile file = QFile(fileName);
    if (!file.open(QIODevice::ReadOnly))
        return;

    auto doc = QJsonDocument::fromJson(file.readAll());

    file.close();

    qDeleteAll(m_robots);
    m_robots.clear();

    auto robotsVariant = doc.toVariant().toList();
    for (const auto &robotVariant : robotsVariant) {
        Robot *robot = new Robot;
        robot->fromQVariant(robotVariant);
        m_robots.append(robot);
    }
}

void MainWindow::fillRobotList()
{
    ui->robotsList->clear();
    for (auto i = 0; i < m_robots.size(); i++)
        ui->robotsList->addItem(QString::number(i + 1) + ": " + m_robots[i]->toQString());

    ui->numOfRobotsLE->setText(QString::number(m_robots.size()));
}

void MainWindow::onBattleClicked()
{
    if (m_robots.isEmpty())
        return;

    ui->loadRobotsBtn->setDisabled(true);
    ui->startBattleBtn->setDisabled(true);

    ui->mostWinsLE->clear();

    initBattleTable();
    startThreads();
}

void MainWindow::initBattleTable()
{
    auto numOfRobots = m_robots.size();

    ui->battlesTable->clear();
    ui->battlesTable->setRowCount(numOfRobots);
    ui->battlesTable->setColumnCount(numOfRobots);

    for (int i = 0; i < numOfRobots; i++) {
        for (int j = 0; j < numOfRobots; j++) {
            auto *tableItem = new QTableWidgetItem;
            if (i == j)
                tableItem->setText("\\");
            ui->battlesTable->setItem(i, j, tableItem);
        }
    }
}

void MainWindow::startThreads()
{
    auto numOfRobots = m_robots.size();

    for (int i = 0; i < numOfRobots; i++) {
        for (int j = i + 1; j < numOfRobots; j++) {
            RobotWorker *worker = new RobotWorker(m_robots[i], m_robots[j], i, j);
            worker->start();
            connect(worker, &QThread::finished, worker, &QObject::deleteLater);
            connect(worker, &RobotWorker::battleFinished, this, &MainWindow::setBattleWinner);
            m_numOfRemainingBattles++;
        }
    }
}

void MainWindow::setBattleWinner(int winner, int loser)
{
    ui->battlesTable->item(winner, loser)->setText("Pobedio");
    ui->battlesTable->item(loser, winner)->setText("Izgubio");
    m_numOfRemainingBattles--;

    if (m_numOfRemainingBattles == 0) {
        setCompetitionWinner();
        ui->loadRobotsBtn->setEnabled(true);
        ui->startBattleBtn->setEnabled(true);
    }
}

void MainWindow::setCompetitionWinner()
{
    auto numOfRobots = m_robots.size();

    int bestIdx = 0, mostWins = 0;
    for (int i = 0; i < numOfRobots; i++) {
        int numOfWins = 0;
        for (int j = 0; j < numOfRobots; j++) {
            if (ui->battlesTable->item(i, j)->text() == "Pobedio") {
                numOfWins++;
            }
        }
        if (numOfWins > mostWins) {
            mostWins = numOfWins;
            bestIdx = i;
        }
    }

    ui->mostWinsLE->setText(m_robots[bestIdx]->getName());
}

