#include "Robot.h"

#include <QRandomGenerator>
#include <utility>

Robot::Robot()
{

}

void Robot::fromQVariant(const QVariant &variant)
{
    auto map = variant.toMap();
    m_name = map.value("name").toString();
    m_maxHitPoints = map.value("hp").toUInt();
    m_currentHitPoints = m_maxHitPoints;

    auto atkList = map.value("atk").toList();
    m_minAttack = atkList[0].toUInt();
    m_maxAttack = atkList[1].toUInt();
}

QString Robot::toQString() const
{
    return m_name + " : hp="
           + QString::number(m_maxHitPoints)
           + ", atk=[" + QString::number(m_minAttack)
           + "," + QString::number(m_maxAttack) + "]";
}

bool Robot::attack(Robot &target) const
{
    unsigned damage = QRandomGenerator::global()->bounded(m_minAttack, m_maxAttack + 1);
    double scaling = std::max(0.5, static_cast<double>(m_currentHitPoints) / m_maxHitPoints);
    unsigned scaledDamage = scaling * damage;

    if (scaledDamage >= target.m_currentHitPoints) {
        target.m_currentHitPoints = 0;
        return true;
    } else {
        target.m_currentHitPoints -= scaledDamage;
        return false;
    }
}

void Robot::resetHitPoints()
{
    m_currentHitPoints = m_maxHitPoints;
}

QMutex *Robot::getBattleMutex()
{
    return &m_battleMutex;
}

QString Robot::getName() const
{
    return m_name;
}
