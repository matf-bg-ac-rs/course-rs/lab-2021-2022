#include "worker.h"
#include "sources.h"
#include <QRandomGenerator>
#include <QMutexLocker>

Worker::Worker(const QVector<Source *>& sources, QMutex *mutex)
    : m_sources(sources)
    , m_mutex(mutex)
{

}

void Worker::run()
{
    while(true){
        auto sleep_time = QRandomGenerator::global()->generate() % 6 + 5;
        msleep(sleep_time * 100u);

        QMutexLocker lock(m_mutex);
        auto i = QRandomGenerator::global()->generate() % m_sources.size();
        auto source = m_sources[i];

        if(!source->isEmpty()){
            auto release_volume = QRandomGenerator::global()->generate() % 101 + 100;
            auto real_released = source->release(release_volume);
            emit released(i, real_released);
        }
    }
}
