#ifndef SOURCES_H
#define SOURCES_H

#include<QString>
#include<QVariant>

class Source
{
public:
    Source();
    Source(const QVariant&);
    void fromQVariant(const QVariant&);
    QString toQString() const;
    bool isEmpty() const;
    unsigned release(unsigned);

private:
    QString m_name;
    unsigned int m_volume;
};

#endif // SOURCES_H
