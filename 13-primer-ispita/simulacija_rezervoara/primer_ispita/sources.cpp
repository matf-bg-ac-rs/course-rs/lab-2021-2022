#include "sources.h"

Source::Source()
{

}

Source::Source(const QVariant &v)
{
    fromQVariant(v);
}

void Source::fromQVariant(const QVariant &variant)
{
    const QVariantMap map = variant.toMap();
    m_name = map.value("name").toString();
    m_volume = map.value("volume").toUInt();
}

QString Source::toQString() const
{
    QString volume = isEmpty()
                   ? "izvor je iscrpljen"
                   : QString::number(m_volume);
    return m_name + " - " + volume;
}

bool Source::isEmpty() const
{
    return m_volume == 0u;
}

unsigned Source::release(unsigned release_volume)
{
    if(m_volume >= release_volume){
        m_volume -= release_volume;
    }
    else{
        release_volume = m_volume;
        m_volume = 0u;
    }
    return release_volume;
}
