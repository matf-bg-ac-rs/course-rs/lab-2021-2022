#ifndef NIT_H
#define NIT_H

#include <QThread>
#include <QMutex>
#include <city.h>

class Nit : public QThread
{
    Q_OBJECT
public:
    Nit(QVector<City*>* cities, QObject* parent = nullptr);

    // QThread interface
protected:
    // Nikako ne preopteretiti start metod
    void run() final;

private:
    QVector<City*>* m_cities;

    // Ukoliko se ne setite optimalnog zakljucavanja
    // Imajte static mutex na nivou svih threadova
    // Manje je optimalno al bar ne dolazi do trke za resursima
    // static QMutex s_mutex;

signals:
    void collected(QString article);
    void distance(unsigned);
};

#endif // NIT_H
