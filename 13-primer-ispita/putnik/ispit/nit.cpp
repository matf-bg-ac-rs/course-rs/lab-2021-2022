#include "nit.h"

#include <QRandomGenerator>
#include <QMutexLocker>


Nit::Nit(QVector<City*>* cities, QObject *parent)
    : QThread(parent), m_cities(cities)
{

}

void Nit::run()
{
    //auto ponavljanja = QRandomGenerator::global()->generate() % 5 + 3;
    auto ponavljanja = QRandomGenerator::global()->bounded(3,7);
    unsigned predjeni_put = 0;
    unsigned city_i;
    for(int i = 0; i != ponavljanja; ++i){
        const auto sleep_time = QRandomGenerator::global()->bounded(250,500);
        msleep(sleep_time);

        // Ukoliko se ne setite optimalnog zakljucavanja
        // Imajte static mutex na nivou svih threadova
        // Manje je optimalno al bar ne dolazi do trke za resursima
        //QMutexLocker lock(&s_mutex);

        if(i == 0){
            city_i = QRandomGenerator::global()->generate() % m_cities->size();
        }
        else{
            // Ako se ne setite optimalnog nacina za biranje neposecene destinacije
            // bilo kako je bolje nego nista
            // auto new_city_i = city_i;
            // while(new_city_i == city_i){
            //    new_city_i = QRandomGenerator::global()->generate() % m_cities->size();
            // }
            // moze i do while

            auto new_city_i = QRandomGenerator::global()->generate() % (m_cities->size() - 1);
            if(new_city_i >= city_i) ++new_city_i;

            predjeni_put += (*m_cities)[city_i]->distance(*(*m_cities)[new_city_i]);

            city_i = new_city_i;
        }

        QMutexLocker lock(&(*m_cities)[city_i]->m_mutex);

        QString article = (*m_cities)[city_i]->getRandomArticle();
        if(!article.isEmpty()){
            emit collected(article);
        }
    }

    emit distance(predjeni_put);
}
