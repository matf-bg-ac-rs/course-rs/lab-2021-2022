#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <city.h>
QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

public slots:
    void loadCities();
    void startVisits();
    void finishedVisit(unsigned);
    void addArticle(QString);

private:
    Ui::Widget *ui;
    QVector<City*> m_cities;
    unsigned active_threads;
    QMap<QString, unsigned> m_collected_articles;

    void readCitiesFromDialog();
    void showCities();
};
#endif // WIDGET_H
