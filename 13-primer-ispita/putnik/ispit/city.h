#ifndef CITY_H
#define CITY_H

#include <QVector>
#include <QString>
#include <QVariant>
#include <QMutex>

class City
{
public:
    City() = default;
    // Nije neophodan ovaj konstruktor
    // ali vam je dozvoljeno da dodajete
    // stagod mislite da bi vam pomoglo
    City(const QVariant&);

    void fromQVariant(const QVariant&);
    QString toQString() const;

    QString getRandomArticle();
    unsigned distance(const City& other) const;

    // Za optimalno zakljucavanje svaki grad ima svoj mutex
    // Takodje u mainwindow mozete imati niz mutexa
    // Ali onda i to prosledjujete niti
    QMutex m_mutex;

private:
    QString m_name;
    unsigned m_x;
    unsigned m_y;
    QVector<QString> m_articles;

    // Pomocna funkcija za apsolutno oduzimanje neoznacenih projeva
    // nije neophodna ali olaksava kod
    // static jer nam ne trebaju instance
    static unsigned abs_minus(unsigned a, unsigned b);
};

#endif // CITY_H
