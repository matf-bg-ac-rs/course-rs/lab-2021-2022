#include "widget.h"
#include "ui_widget.h"

#include <QFileDialog>
#include <QJsonDocument>

#include <nit.h>

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    connect(ui->pbUcitaj, &QPushButton::clicked, this, &Widget::loadCities);
    connect(ui->pbZapocni, &QPushButton::clicked, this, &Widget::startVisits);
}

Widget::~Widget()
{
    delete ui;
    qDeleteAll(m_cities);
}

void Widget::loadCities()
{
    readCitiesFromDialog();
    showCities();
}

void Widget::startVisits()
{
    if(m_cities.empty()) return;

    bool ispravan;
    const unsigned broj_posetioca = ui->lePosetioci->text().toUInt(&ispravan);
    if(!ispravan || broj_posetioca == 0) return;

    // Obe varijante rade
    ui->pbUcitaj->setDisabled(true);
    ui->pbZapocni->setEnabled(false);

    ui->lePut->clear();
    ui->lwArtikli->clear();

    active_threads = broj_posetioca;
    for(unsigned i = 0; i != broj_posetioca; ++i){
        auto thread = new Nit(&m_cities , this);
        connect(thread, &QThread::finished, thread, &QObject::deleteLater);
        connect(thread, &Nit::distance, this, &Widget::finishedVisit);
        connect(thread, &Nit::collected, this, &Widget::addArticle);

        // Nikako ne pokrenuti run metod
        thread->start();
    }
}

void Widget::finishedVisit(unsigned int distance)
{
    auto current_distance = ui->lePut->text().toUInt();
    ui->lePut->setText(QString::number(current_distance + distance));

    --active_threads;
    if(active_threads==0){
        ui->pbUcitaj->setDisabled(false);
        ui->pbZapocni->setEnabled(true);
    }
}

void Widget::addArticle(QString article)
{
    if(m_collected_articles.contains(article)){
        m_collected_articles[article] += 1;
    }
    else {
        m_collected_articles[article] = 1;
    }

    ui->lwArtikli->clear();
    for(auto item : m_collected_articles.toStdMap()){
        const auto text = item.first + ": " + QString::number(item.second);
        ui->lwArtikli->addItem(text);
    }
}

void Widget::readCitiesFromDialog()
{
    auto files = QFileDialog::getOpenFileNames(this, "Naslov", "..", "JSON(*.json)");

    // Ako se ne setite qDeleteAll:
    //for(auto city : m_cities) delete city;
    qDeleteAll(m_cities);
    m_cities.clear();

    for(auto& filePath : files){
        QFile file(filePath);
        if(file.open(QFile::ReadOnly)){
            const auto contents = file.readAll();

            const auto doc = QJsonDocument::fromJson(contents);
            const auto var = doc.toVariant();

            m_cities.push_back(new City(var));

            // Ako niste napravili konstruktor
            // nije neophodan
            // auto city = new City;
            // city->fromQVariant(var);
            // m_cities.push_back(city);
        }
    }
}

void Widget::showCities()
{
    ui->lwGradovi->clear();

    for(auto city : m_cities){
        ui->lwGradovi->addItem(city->toQString());
        // Ako hocete rucno da pravite item:
        // auto item = new QListWidgetItem(city->toQString());
        // ui->lwGradovi->addItem(item);
    }
}

