#include "city.h"

#include <QRandomGenerator>

City::City(const QVariant &var)
{
    fromQVariant(var);
}

void City::fromQVariant(const QVariant &v)
{
    const auto map = v.toMap();

    m_name = map["name"].toString();

    const auto coords = map["coordinates"].toList();
    m_x = coords[0].toUInt();
    m_y = coords[1].toUInt();

    m_articles.clear();
    auto articles = map["articles"].toList();
    for(auto& article : articles){
        m_articles.push_back(article.toString());
    }
}

QString City::toQString() const
{
    // Ako probamo da samo stavimo "(" bez QString konstruktora
    // promenljiva se tretira kao char*
    // pa ce naredno sabiranje moze biti sabiranje pokazivaca
    // u zavisnosti od tipa drugog argumenta
    const auto pozicija = QString("(")
                          + QString::number(m_x)
                          + ", "
                          + QString::number(m_y)
                          + ")";
    return m_name
           + " "
           + pozicija
           + ": Broj artikala = "
           + QString::number(m_articles.size());
}

QString City::getRandomArticle()
{
    if(m_articles.empty()){
        return "";
    }

    const auto i = QRandomGenerator::global()->generate() % m_articles.size();
    const auto article = m_articles[i];

    m_articles.remove(i);

//    Ako ne mozete da se setite metoda remove:
//    for(auto j = i; j < m_articles.size() - 2; j++){
//        m_articles[j] = m_articles[j+1];
//    }
//    m_articles.pop_back();

    return article;
}

unsigned int City::distance(const City &other) const
{
    // Obican abs vam nece pomoci jer se oduzimaju neoznaceni brojevi
    // rezultat ce svakako biti pozitivan broj
    // jer ce doci do potkoracenja
    return abs_minus(m_x, other.m_x) + abs_minus(m_y, other.m_y);
    // Ako niste izdvojili moze:
    // return ((m_x > other.m_x)?(m_x - other.m_x):(other.m_x - m_x))
    //        + ((m_y > other.m_y)?(m_y - other.m_y):(other.m_y - m_y));
}

unsigned int City::abs_minus(unsigned int a, unsigned int b)
{
    return a > b ? a-b : b-a;
}
