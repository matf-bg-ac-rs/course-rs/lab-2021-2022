#pragma once

#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>

using namespace std;

void brojevi()
{
    vector<int> xs;

    copy(istream_iterator<int>(cin), istream_iterator<int>(), back_inserter(xs));
    sort(begin(xs), end(xs));
    copy(cbegin(xs), cend(xs), ostream_iterator<int>(cout, " "));
    cout << endl;
    copy(crbegin(xs), crend(xs), ostream_iterator<int>(cout, " "));
    cout << endl;
}