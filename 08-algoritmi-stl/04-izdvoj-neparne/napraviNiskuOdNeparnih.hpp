#pragma once

#include <vector>
#include <numeric>
#include <iostream>
#include <string>

using namespace std;

string napraviNiskuOdNeparnih(const vector<int> &brojevi)
{
    return accumulate(cbegin(brojevi), cend(brojevi), string(), [](const auto &akumulator, const auto &broj)
                      { return broj % 2 != 0 ? (akumulator + to_string(broj)) : akumulator; });
}
