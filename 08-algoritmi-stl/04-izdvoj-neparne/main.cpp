#include "napraviNiskuOdNeparnih.hpp"

using namespace std;

int main()
{
    cout << napraviNiskuOdNeparnih({-5, -4, -3, -2, -1, 0, 1, 2, 3}) << endl;

    return 0;
}
