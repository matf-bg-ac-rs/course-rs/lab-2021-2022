#pragma once

#include <iostream>
#include <algorithm>
#include <string>
#include <iterator>

using namespace std;

bool palindrom(const string &str)
{
    // Napomena: Algoritam equal ne proverava da li su intervali jednaki.
    // U ovom slucaju, to nije neophodno.
    return equal(cbegin(str), cend(str), crbegin(str));
}
