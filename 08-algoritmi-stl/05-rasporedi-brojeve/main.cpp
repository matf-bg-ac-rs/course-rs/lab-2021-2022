#include <vector>
#include <iostream>
#include <algorithm>
#include <iterator>
#include "rasporediBrojeve.hpp"

using namespace std;

int main()
{
    vector<int> brojevi{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
    const auto [krajPrveParticije, krajDrugeParticije] = rasporediBrojeve(brojevi);

    cout << "Deljivi brojem 3:" << endl;
    copy(cbegin(brojevi), krajPrveParticije, ostream_iterator<int>(cout, " "));
    cout << "\nDaju ostatak 1 deljenjem brojem 3:" << endl;
    copy(krajPrveParticije, krajDrugeParticije, ostream_iterator<int>(cout, " "));
    cout << "\nDaju ostatak 2 deljenjem brojem 3:" << endl;
    copy(krajDrugeParticije, cend(brojevi), ostream_iterator<int>(cout, " "));
    cout << endl;

    return 0;
}
