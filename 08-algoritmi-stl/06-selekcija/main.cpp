#include <vector>
#include <iostream>
#include <algorithm>
#include <iterator>
#include <string>
#include "selekcija.hpp"

using namespace std;

int main()
{
    vector<string> stavke1{
        "aardvark",
        "compunctious",
        "**congratulations**", // +2 --> pocetak
        "**contrafribblarity**",
        "**contrary**",
        "dictionary", // +5 --> kraj
        "interphrastical",
        "patronise", // +7 --> odrediste
        "pericombobulation",
        "phrasmotic",
        "syllables"};

    slide_selection(begin(stavke1) + 2,
                    begin(stavke1) + 5,
                    begin(stavke1) + 7);

    copy(begin(stavke1), end(stavke1), ostream_iterator<string>(cout, "\n"));

    cout << "---" << endl;

    vector<string> stavke2 = {
        "aardvark", // +0 --> odrediste
        "compunctious",
        "dictionary",
        "interphrastical",
        "**congratulations**", // +4 --> pocetak
        "**contrafribblarity**",
        "**contrary**",
        "patronise", // +7--> kraj
        "pericombobulation",
        "phrasmotic",
        "syllables"};

    slide_selection(begin(stavke2) + 4,
                    begin(stavke2) + 7,
                    begin(stavke2) + 0);

    copy(begin(stavke2), end(stavke2), ostream_iterator<string>(cout, "\n"));

    cout << "---" << endl;

    vector<string> stavke3 = {
        "aardvark",
        "compunctious",
        "dictionary",
        "interphrastical",
        "**congratulations**", // +4 --> pocetak
        "**contrafribblarity**",
        "**contrary**",
        "**patronise**", // +7 --> odrediste
        "**pericombobulation**",
        "phrasmotic", // +9 --> kraj
        "syllables"};

    slide_selection(begin(stavke3) + 4,
                    begin(stavke3) + 9,
                    begin(stavke3) + 7);

    copy(begin(stavke3), end(stavke3), ostream_iterator<string>(cout, "\n"));

    return 0;
}
