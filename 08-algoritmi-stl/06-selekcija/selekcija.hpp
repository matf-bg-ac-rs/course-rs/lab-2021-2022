#pragma once

#include <vector>
#include <iostream>
#include <algorithm>
#include <iterator>
#include <string>

using namespace std;

using StringIter = vector<string>::iterator;

pair<StringIter, StringIter> slide_selection(StringIter pocetak, StringIter kraj, StringIter odrediste)
{
    if (odrediste < pocetak)
    {
        return {odrediste, rotate(odrediste, pocetak, kraj)};
    }

    if (kraj < odrediste)
    {
        return {rotate(pocetak, kraj, odrediste), odrediste};
    }

    const auto first = pocetak - (kraj - odrediste);
    return {first, rotate(first, pocetak, kraj)};
}
