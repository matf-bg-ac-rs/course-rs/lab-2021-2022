#pragma once

#include <iostream>
#include <string>
#include <numeric>
#include <vector>
#include <iterator>

using namespace std;

int brojUzastopnihJednakih(const vector<int> &brojevi)
{
    return inner_product(
        cbegin(brojevi),
        cend(brojevi) - 1,
        cbegin(brojevi) + 1,
        int(),
        [](const auto &akumulator, const auto &broj)
        { return akumulator + broj; },
        [](const auto &leviBroj, const auto &desniBroj)
        { return leviBroj == desniBroj ? 1 : 0; });
}
