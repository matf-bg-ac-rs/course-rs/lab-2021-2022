#include <iostream>
#include "proveriUredjenost.hpp"

using namespace std;

int main()
{
    cout << "abcdefghijklmnopqrstuvwxyz"
         << ": " << proveriUredjenost("abcdefghijklmnopqrstuvwxyz") << endl;
    cout << "almosT"
         << ": " << proveriUredjenost("almosT") << endl;

    return 0;
}
